package analyze

import (
	"context"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"sort"

	"codeberg.org/korve/phissile/pkg"
)

var configFilesNames = map[string]int{
	".phpcs.xml":      0,
	"phpcs.xml":       1,
	".phpcs.xml.dist": 2,
	"phpcs.xml.dist":  3,
}

// The PHPCSAnalyzer checks if a project is a PHPCS projects.
type PHPCSAnalyzer struct {
}

// Analyze checks if the project at path is a PHPCS projects. As per the
// PHPCS Documentation with the following logic: If you run PHP_CodeSniffer
// without specifying a coding standard, PHP_CodeSniffer will look in the
// current directory, and all parent directories, for a file called either
// .phpcs.xml, phpcs.xml, .phpcs.xml.dist, or phpcs.xml.dist. If found,
// configuration information will be read from this file, including the files to
// check, the coding standard to use, and any command line arguments to apply.
func (p *PHPCSAnalyzer) Analyze(_ context.Context, path string) (*pkg.Project, error) {
	project := &pkg.Project{}

	err := pkg.WalkUp(path, func(p string) error {
		stats, err := ioutil.ReadDir(p)
		if err != nil {
			return err
		}

		var configFiles []string
		for _, stat := range stats {
			name := stat.Name()

			if _, ok := configFilesNames[name]; ok {
				configFiles = append(configFiles, filepath.Join(p, name))
			}
		}

		if len(configFiles) > 0 {
			// When multiple config files are present in the same directory the first is
			// selected in the following order: .phpcs.xml, phpcs.xml, .phpcs.xml.dist,
			// phpcs.xml.dist
			//
			// Documented here:
			// https://github.com/squizlabs/PHP_CodeSniffer/wiki/Advanced-Usage#using-a-default-configuration-file
			if len(configFiles) > 1 {
				sort.Slice(configFiles, func(i, j int) bool {
					return configFilesNames[configFiles[i]] < configFilesNames[configFiles[j]]
				})
			}

			project.ConfigFiles = configFiles[:1]
			project.Types = []pkg.ProjectType{pkg.ProjectTypePhpcs}

			// break walking up the directory tree.
			return &pkg.AbortError{}
		}

		return nil
	}, false)

	if err != nil {
		return nil, fmt.Errorf("failed to walk up the directory tree while searching for phpcs config files: %w", err)
	}

	if len(project.Types) == 0 {
		return nil, NewUnknownProjectTypeError(path)
	}

	return project, nil
}
