package analyze

import (
	"context"
	"errors"
	"sort"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"codeberg.org/korve/phissile/pkg"
)

type testProjectAnalyzer struct {
	error         error
	project       *pkg.Project
	analyzeCalled bool
	analyzePath   string
}

func (t *testProjectAnalyzer) Analyze(_ context.Context, path string) (*pkg.Project, error) {
	t.analyzeCalled = true
	t.analyzePath = path
	return t.project, t.error
}

func TestAggregateProjectAnalyzer_Analyze(t *testing.T) {
	type fields struct {
		innerAnalyzers []ProjectAnalyzer
	}
	type args struct {
		path string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *pkg.Project
		wantErr bool
	}{
		{
			"simple test case",
			fields{
				innerAnalyzers: []ProjectAnalyzer{
					&testProjectAnalyzer{
						project: &pkg.Project{
							Types: []pkg.ProjectType{pkg.ProjectTypePhpcs},
						},
					},
					&testProjectAnalyzer{
						project: &pkg.Project{
							Types: []pkg.ProjectType{pkg.ProjectTypePsalm},
						},
					},
				},
			},
			args{
				"foo/bar",
			},
			&pkg.Project{
				Types:       []pkg.ProjectType{pkg.ProjectTypePhpcs, pkg.ProjectTypePsalm},
				ConfigFiles: []string{},
			},
			false,
		},
		{
			"unknown error should fail",
			fields{
				innerAnalyzers: []ProjectAnalyzer{
					&testProjectAnalyzer{
						project: &pkg.Project{
							Types: []pkg.ProjectType{pkg.ProjectTypePhpcs},
						},
					},
					&testProjectAnalyzer{
						error: errors.New("failed"),
					},
				},
			},
			args{
				"foo/bar",
			},
			nil,
			true,
		},
		{
			"analyzer should skip UnknownProjectTypeError",
			fields{
				innerAnalyzers: []ProjectAnalyzer{
					&testProjectAnalyzer{
						project: &pkg.Project{
							Types: []pkg.ProjectType{pkg.ProjectTypePhpcs},
						},
					},
					&testProjectAnalyzer{
						error: &UnknownProjectTypeError{Path: "foo/bar"},
					},
				},
			},
			args{
				"foo/bar",
			},
			&pkg.Project{
				Types:       []pkg.ProjectType{pkg.ProjectTypePhpcs},
				ConfigFiles: []string{},
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			a := NewAggregateProjectAnalyzer(tt.fields.innerAnalyzers)
			got, err := a.Analyze(ctx, tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("Analyze() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !tt.wantErr && got != nil {
				sort.Slice(got.Types, func(i, j int) bool {
					return got.Types[i] < got.Types[j]
				})

				assert.EqualValues(t, tt.want.Types, got.Types)

				for _, analyzer := range a.innerAnalyzers {
					testAnalyzer, ok := analyzer.(*testProjectAnalyzer)

					assert.True(t, ok)
					assert.True(t, testAnalyzer.analyzeCalled)
				}
			}
		})
	}
}
