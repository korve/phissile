package analyze

import (
	"context"
	"fmt"

	"codeberg.org/korve/phissile/pkg"
)

// UnknownProjectTypeError is returned when analyzing a project yielded no known
// project types.
type UnknownProjectTypeError struct {
	// Path is the project root that has been anaylzed.
	Path string
}

func (u *UnknownProjectTypeError) Error() string {
	return fmt.Sprintf("analyzer: unknown project type at %s", *u)
}

// IsUnknownProjectTypeError checks if err is a UnknownProjectTypeError.
func IsUnknownProjectTypeError(err error) bool {
	if _, ok := err.(*UnknownProjectTypeError); ok {
		return true
	}
	return false
}

// NewUnknownProjectTypeError is the UnknownProjectTypeError constructor
func NewUnknownProjectTypeError(path string) *UnknownProjectTypeError {
	return &UnknownProjectTypeError{Path: path}
}

// ProjectAnalyzer analyzes a directory for configuration files that identify a
// certain code style framework.
type ProjectAnalyzer interface {
	// Analyze analyzes path for code style frameworks. The path must be an absolute
	// path
	//
	// Errors:
	//  UnknownProjectTypeError When no project type could be determined
	Analyze(ctx context.Context, path string) (*pkg.Project, error)
}
