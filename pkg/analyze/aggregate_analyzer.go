package analyze

import (
	"context"
	"sync"

	"codeberg.org/korve/phissile/pkg"
)

// The AggregateProjectAnalyzer calls all inner ProjectAnalyzers to see if any of them returns a valid project.
// All valid projects are then
type AggregateProjectAnalyzer struct {
	innerAnalyzers []ProjectAnalyzer
}

// Analyze analyzes a project using multiple inner analyzers. Analyze will then call Analyze on each each inner analyzer and
// combine the results to a final project. When one analyzer fails with a non UnknownProjectTypeError the error will be returned
// and project analyzing will be stopped immediately. path can be a relative path.
func (a *AggregateProjectAnalyzer) Analyze(ctx context.Context, path string) (*pkg.Project, error) {
	project := &pkg.Project{}
	wg := sync.WaitGroup{}
	m := sync.Mutex{}

	errors := make(chan error, len(a.innerAnalyzers))

	for _, analyzer := range a.innerAnalyzers {
		wg.Add(1)

		analyzer := analyzer

		go func() {
			defer wg.Done()

			p, err := analyzer.Analyze(ctx, path)
			if err != nil {
				if !IsUnknownProjectTypeError(err) {
					errors <- err
				}
				return
			}

			m.Lock()
			project.ConfigFiles = append(project.ConfigFiles, p.ConfigFiles...)
			project.Types = append(project.Types, p.Types...)
			m.Unlock()
		}()
	}

	wg.Wait()

	if len(errors) > 0 {
		return nil, <-errors
	}

	return project, nil
}

// NewAggregateProjectAnalyzer creates a new AggregateProjectAnalyzer instance. Provide a list of analyzers
// and the AggregateAnalyzer will call Analyze on all of them when AggregateProjectAnalyzer.Analyze is called on it.
func NewAggregateProjectAnalyzer(innerAnalyzers []ProjectAnalyzer) *AggregateProjectAnalyzer {
	return &AggregateProjectAnalyzer{innerAnalyzers}
}
