package analyze

import (
	"context"
	"log"
	"path"
	"path/filepath"
	"testing"

	"github.com/rs/xid"
	"github.com/stretchr/testify/assert"

	"codeberg.org/korve/phissile/pkg"
)

func TestPHPCSAnalyzer_Analyze(t *testing.T) {
	type args struct {
		path string
	}

	projectType := []pkg.ProjectType{pkg.ProjectTypePhpcs}
	tests := []struct {
		name    string
		args    args
		want    *pkg.Project
		wantErr bool
	}{
		{"phpcs phpcs.xml",
			args{"testdata/analyzer/phpcs/phpcs_xml"},
			&pkg.Project{projectType, []string{absPath("testdata/analyzer/phpcs/phpcs_xml/phpcs.xml")}},
			false,
		},
		{"phpcs .phpcs.xml",
			args{"testdata/analyzer/phpcs/_phpcs_xml"},
			&pkg.Project{projectType, []string{absPath("testdata/analyzer/phpcs/_phpcs_xml/.phpcs.xml")}},
			false,
		},
		{"phpcs phpcs.xml.dist",
			args{"testdata/analyzer/phpcs/phpcs_xml_dist"},
			&pkg.Project{projectType, []string{absPath("testdata/analyzer/phpcs/phpcs_xml_dist/phpcs.xml.dist")}},
			false,
		},
		{"phpcs .phpcs.xml.dist",
			args{"testdata/analyzer/phpcs/_phpcs_xml_dist"},
			&pkg.Project{projectType, []string{absPath("testdata/analyzer/phpcs/_phpcs_xml_dist/.phpcs.xml.dist")}},
			false,
		},
		{"multiple files",
			args{"testdata/analyzer/phpcs/multi"},
			&pkg.Project{projectType, []string{
				absPath("testdata/analyzer/phpcs/multi/.phpcs.xml")}},
			false,
		},
		{"multiple files nested",
			args{"testdata/analyzer/phpcs/multi_nested/a/b/c/d"},
			&pkg.Project{projectType, []string{
				absPath("testdata/analyzer/phpcs/multi_nested/a/b/c/d/.phpcs.xml")}},
			false,
		},
		{"no project",
			args{"testdata/analyzer/phpcs/no_project"},
			nil,
			true,
		},
		{"dir does not exist",
			args{path.Join("testdata/analyzer/phpcs", xid.New().String())},
			nil,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &PHPCSAnalyzer{}
			got, err := a.Analyze(context.Background(), tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("Analyze() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			assert.Equal(t, tt.want, got)
		})
	}
}

func absPath(p string) string {
	abs, err := filepath.Abs(p)
	if err != nil {
		log.Fatal(err)
	}
	return abs
}
