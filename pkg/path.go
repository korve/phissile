package pkg

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
)

// The AbortError can be returned by a WalkUp function to signal that the
// walking should be stopped.
type AbortError struct {
}

func (a *AbortError) Error() string {
	return "aborted"
}

func walk(p string, cb func(p string) error) error {
	if err := cb(p); err != nil {
		if errors.Is(err, &AbortError{}) {
			return nil
		}
		return err
	}

	if p == "/" {
		return nil
	}

	parent := filepath.Join(p, "..")
	return walk(parent, cb)
}

// WalkUp walks up the directory tree until it reaches the root. The callback
// function will be called for every directory on the way up with its absolute
// path. To cancel the walking the callback can return an AbortError to signal
// that the walking hasn't been stopped by a real error.
func WalkUp(p string, cb func(p string) error, followSymlinks bool) error {
	absPath, err := filepath.Abs(p)
	if err != nil {
		return err
	}

	stat, err := os.Stat(absPath)
	if err != nil {
		return err
	}

	if !stat.IsDir() {
		return fmt.Errorf("\"%s\" is not a directory", p)
	}

	if followSymlinks {
		absPath, err = filepath.EvalSymlinks(absPath)
		if err != nil {
			return err
		}
	}

	return walk(absPath, cb)
}
