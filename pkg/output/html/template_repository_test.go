package html

import (
	"html/template"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"

	"codeberg.org/korve/phissile/pkg"
)

func TestThemeTemplateRepository_GetTemplate(t1 *testing.T) {
	theme := DefaultTheme

	emptyFunc := func() string { return "" }

	funcMap := pkg.CopyMap(defaultFuncMap)
	funcMap["renderRule"] = emptyFunc

	for k := range funcMap {
		funcMap[k] = emptyFunc
	}

	tmpl := template.Must(
		template.New("theme").Funcs(funcMap).ParseFS(
			embedFs,
			filepath.Join(theme.Path(), "*"),
		),
	)

	type fields struct {
		theme     ThemePath
		funcMap   *map[string]interface{}
		templates *template.Template
	}
	type args struct {
		name string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *template.Template
	}{
		{"index", fields{theme: theme, funcMap: &funcMap}, args{"index.gohtml"}, tmpl.Lookup("index.gohtml")},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := &ThemeTemplateRepository{
				theme:     tt.fields.theme,
				funcMap:   tt.fields.funcMap,
				templates: tt.fields.templates,
			}

			got := t.GetTemplate(tt.args.name)
			assert.Equal(t1, tt.want.Name(), got.Name())
		})
	}
}
