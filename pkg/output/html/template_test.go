package html

import (
	"html/template"
	"math/big"
	"testing"

	"codeberg.org/korve/phissile/pkg/i18n"
)

func TestMain(m *testing.M) {
	i18n.RegisterTranslations(&map[string]string{
		"foo": "hello world",
		"bar": "hello {{.baz}}",
	}, i18n.DefaultLocale)

	m.Run()
}

func Test_translate(t *testing.T) {
	type args struct {
		key    string
		params []string
	}
	tests := []struct {
		name    string
		args    args
		want    template.HTML
		wantErr bool
	}{
		{"simple key", args{"foo", []string{}}, "hello world", false},
		{"with arguments", args{"bar", []string{"baz", "world"}}, "hello world", false},
		{"with invalid arguments", args{"bar", []string{"baz"}}, "", true},
		{"with missing translations", args{"non-existent", []string{}}, "", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := translate(tt.args.key, tt.args.params...)
			if (err != nil) != tt.wantErr {
				t.Errorf("translate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("translate() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_toString(t *testing.T) {
	type args struct {
		val interface{}
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{"int", args{100000}, "100000", false},
		{"int32", args{int32(100000)}, "100000", false},
		{"scientific notation", args{1e5}, "100000.0000", false},
		{"float32", args{float32(1.5)}, "1.5000", false},
		{"float64", args{1.5}, "1.5000", false},
		{"rat", args{big.NewRat(1, 2)}, "1/2", false},
		{"string", args{"foo"}, "foo", false},
		{"uint", args{uint(5)}, "5", false},
		{"uint32", args{uint32(5)}, "5", false},
		{"uint64", args{uint64(5)}, "5", false},
		{"bool true", args{true}, "true", false},
		{"bool false", args{false}, "false", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := toString(tt.args.val)
			if (err != nil) != tt.wantErr {
				t.Errorf("toString() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("toString() got = %v, want %v", got, tt.want)
			}
		})
	}
}
