package html

import (
	"bytes"
	"fmt"
	"sync"

	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/output"
)

const (
	// DefaultTheme is the theme that is used when no theme was provided to the
	// Generator constructor.
	DefaultTheme Theme = "default"
)

// Generator will generate a html document. Use the constructor to create a new instance.
type Generator struct {
	project            *pkg.Project
	rules              []pkg.Rule
	theme              ThemePath
	funcMap            *map[string]interface{}
	templateRepository TemplateRepository
}

func (g *Generator) generateHTML() (*output.Artifact, error) {
	var buf []byte
	artifact := &output.Artifact{
		Path: "index.html",
		Data: bytes.NewBuffer(buf),
	}

	ctx := &IndexPage{
		Title:   "Styleguide",
		Project: g.project,
		Rules:   g.rules,
	}

	t := g.templateRepository.GetTemplate("index.gohtml")
	if t == nil {
		return nil, fmt.Errorf("index.gohtml template not found")
	}

	if err := t.ExecuteTemplate(artifact.Data, "index.gohtml", ctx); err != nil {
		return nil, err
	}

	return artifact, nil
}

// Generate generates a HTML styleguide and outputs its artifacts to the output
// channel. If an error occurs its written to the error channel. When done true
// is written to the done channel.
func (g *Generator) Generate() (chan *output.Artifact, chan error, chan bool) {
	out := make(chan *output.Artifact)
	errs := make(chan error)
	done := make(chan bool)

	generate := func(wg *sync.WaitGroup, fn func() (*output.Artifact, error)) {
		go func() {
			a, err := fn()
			if err != nil {
				errs <- err
				return
			}

			out <- a
			wg.Done()
		}()
	}

	go func() {
		wg := &sync.WaitGroup{}
		wg.Add(1)
		go generate(wg, g.generateHTML)
		wg.Wait()

		done <- true
	}()

	return out, errs, done
}

// NewGenerator generates a new HTML generator.
func NewGenerator(project *pkg.Project, rules []pkg.Rule, theme ThemePath) *Generator {
	funcMap := pkg.CopyMap(defaultFuncMap)

	templateRepository := &ThemeTemplateRepository{
		theme:   theme,
		funcMap: &funcMap,
	}

	g := &Generator{
		project:            project,
		rules:              rules,
		theme:              theme,
		funcMap:            &funcMap,
		templateRepository: templateRepository,
	}

	renderer := NewDefaultRuleRenderer(theme, templateRepository)
	funcMap["renderRule"] = renderer.Render

	return g
}
