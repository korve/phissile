package html

import "testing"

func TestTheme_Path(t *testing.T) {
	tests := []struct {
		name string
		t    Theme
		want string
	}{
		{"valid path", "foo", "templates/themes/foo"},
		{"empty path", "", "templates/themes"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.t.Path(); got != tt.want {
				t.Errorf("Path() = %v, want %v", got, tt.want)
			}
		})
	}
}
