package html

import (
	"bytes"
	"errors"
	"html/template"
	"reflect"

	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/strings"
)

// RuleRenderer renders a pkg.Rule as a HTML template. The output of the render
// describes the rules effects in a human readable manner.
type RuleRenderer interface {
	Render(rule pkg.Rule) (template.HTML, error)
}

// DefaultRuleRenderer renders rules by rendering templates located in the
// themes rules directory.
type DefaultRuleRenderer struct {
	theme              ThemePath
	templateRepository TemplateRepository
}

// Render is responsible for rendering a pkg.Rule to a HTML string. For most
// rules it guesses the template name from the rule name. For example a rule
// named LineEnding will be translated to "line_ending.gohtml". The template
// must be located in the themes rules folder (for example
// `templates/themes/default/rules`)
func (d *DefaultRuleRenderer) Render(rule pkg.Rule) (template.HTML, error) {
	// This switch can be used to render more specialized templates for a rule.
	// Right now only the default rendering process is used.
	switch rule.(type) {

	// default renderer
	default:
		name, err := ruleName(rule)
		if err != nil {
			return "", err
		}

		t := d.templateRepository.GetTemplate(name)
		wr := bytes.NewBuffer([]byte{})

		if err := t.Execute(wr, rule); err != nil {
			return "", err
		}

		return template.HTML(wr.Bytes()), nil
	}
}

// NewDefaultRuleRenderer creates a new DefaultRuleRenderer. A Theme and a
// FuncMap must be provided so that the Renderer is able to locate the templates
// and use the same template functions that are available in the main template.
func NewDefaultRuleRenderer(theme ThemePath, templateRepository TemplateRepository) *DefaultRuleRenderer {
	return &DefaultRuleRenderer{
		theme:              theme,
		templateRepository: templateRepository,
	}
}

// ruleName converts the rules name to a snake_case representation and appends
// .gohtml to the result. This is used to generate a rules template path.
func ruleName(rule pkg.Rule) (string, error) {
	if rule == nil {
		return "", errors.New("rule is nil")
	}

	name := reflect.ValueOf(rule).Elem().Type().Name()
	return strings.ToSnakeCase(name) + ".gohtml", nil
}
