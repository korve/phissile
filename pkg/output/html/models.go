package html

import "codeberg.org/korve/phissile/pkg"

// IndexPage holds the data for the index.gohtml template.
type IndexPage struct {
	// Title is the HTML <title> and the first headline in the generated HTML
	// document.
	Title string

	// Project is the current projects that has been scanned.
	Project *pkg.Project

	// The set of rules that have been determined.
	Rules []pkg.Rule
}
