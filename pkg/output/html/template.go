package html

import (
	"fmt"
	"html/template"
	"math/big"
	"strconv"

	"codeberg.org/korve/phissile/pkg/i18n"
)

var defaultFuncMap = map[string]interface{}{
	"translate": translate,
	"toString":  toString,
}

// translate is a template func that translates a string by calling i18n.Trans.
// A params map is created by taking the rest of the arguments and pairing them.
// The first element in the pair is the translation key and the second is the
// value. When the number of additional arguments is not even pairs can not be
// generated and an error is thrown.
//
// Examples
//
// 		Assume the "foo" translation is "foo {{ .bar }}"
//
// 		{{ translate "foo" "bar" "baz }} {# Output: "foo baz" #}
func translate(key string, params ...string) (template.HTML, error) {
	var paramsMap map[string]string
	if len(params) > 0 {
		if len(params)%2 != 0 {
			return "", fmt.Errorf("number of arguments must be even")
		}

		paramsMap = make(map[string]string, len(params)/2)
		for i := 0; i < len(params); i += 2 {
			paramsMap[params[i]] = params[i+1]
		}
	}

	trans, err := i18n.Trans(key, i18n.DefaultLocale, paramsMap)
	if err != nil {
		return "", err
	}

	return template.HTML(trans), nil
}

// toString converts a value to a string with some sane defaults. All numbers
// are converted to base 10 with bitSize 64 (if possible). Floats are printed
// with 4 decimal places.
func toString(val interface{}) (string, error) {
	switch v := val.(type) {
	case string:
		return v, nil

	case fmt.Stringer:
		return v.String(), nil

	case bool:
		return strconv.FormatBool(v), nil

	case int:
		return strconv.Itoa(v), nil

	case int32:
		return strconv.Itoa(int(v)), nil

	case int64:
		return strconv.FormatInt(v, 10), nil

	case uint:
		return strconv.FormatUint(uint64(v), 10), nil

	case uint32:
		return strconv.FormatUint(uint64(v), 10), nil

	case uint64:
		return strconv.FormatUint(v, 10), nil

	case float32:
		return strconv.FormatFloat(float64(v), 'f', 4, 64), nil

	case float64:
		return strconv.FormatFloat(v, 'f', 4, 64), nil

	case *big.Rat:
		return v.FloatString(4), nil
	}

	return "", fmt.Errorf("unsupported type %t", val)
}
