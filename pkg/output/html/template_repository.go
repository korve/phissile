package html

import (
	"embed"
	"html/template"
	"path/filepath"
)

//go:embed templates/themes/*/*
var embedFs embed.FS

// A TemplateRepository is responsible for loading and parsing templates.
type TemplateRepository interface {
	// GetTemplate returns the template for name.
	GetTemplate(name string) *template.Template
}

// The ThemeTemplateRepository loads embedded templates for a theme. The themes
// are loaded from the templates/themes/* directories.
type ThemeTemplateRepository struct {
	theme     ThemePath
	funcMap   *map[string]interface{}
	templates *template.Template
}

func (t *ThemeTemplateRepository) ensureParsed() {
	if t.templates == nil {
		t.templates = template.Must(
			template.New("theme").Funcs(*t.funcMap).ParseFS(
				embedFs,
				filepath.Join(t.theme.Path(), "*"),
			),
		)
	}
}

// GetTemplate loads an embedded template. The name must be a file name within
// the theme's directory. E.g. if there is a `index.gohtml` in the theme name
// must also be index.html.
func (t *ThemeTemplateRepository) GetTemplate(name string) *template.Template {
	t.ensureParsed()
	return t.templates.Lookup(name)
}
