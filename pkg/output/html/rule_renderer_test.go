package html

import (
	"errors"
	"html/template"
	"testing"

	"codeberg.org/korve/phissile/pkg"
)

type testTheme struct{}

func (t *testTheme) Path() string {
	return "testdata/"
}

type testRule struct {
	name string
}

func (t *testRule) String() string {
	return t.name
}

type unknownRule struct{}

func (t *unknownRule) String() string {
	return "unknown"
}

type testTemplateRepository struct {
}

func (t *testTemplateRepository) GetTemplate(name string) *template.Template {
	return template.Must(template.New(name).Funcs(
		map[string]interface{}{
			"renderRule": func(rule pkg.Rule) (string, error) {
				switch rule.(type) {
				case *testRule:
					return rule.String(), nil
				default:
					return "", errors.New("unknown rule")
				}
			},
		},
	).Parse("<p>{{renderRule .}}</p>"))
}

func TestDefaultRuleRenderer_Render(t *testing.T) {
	th := &testTheme{}

	type fields struct {
		theme              ThemePath
		templateRepository TemplateRepository
	}
	type args struct {
		rule pkg.Rule
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    template.HTML
		wantErr bool
	}{
		{"test rule", fields{theme: th, templateRepository: &testTemplateRepository{}}, args{rule: &testRule{"foo"}}, "<p>foo</p>", false},
		{"unknown rule", fields{theme: th, templateRepository: &testTemplateRepository{}}, args{rule: &unknownRule{}}, "", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DefaultRuleRenderer{
				theme:              tt.fields.theme,
				templateRepository: tt.fields.templateRepository,
			}
			got, err := d.Render(tt.args.rule)
			if (err != nil) != tt.wantErr {
				t.Errorf("Render() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Render() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_ruleName(t *testing.T) {
	type args struct {
		rule pkg.Rule
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{"valid rule", args{rule: &testRule{}}, "test_rule.gohtml", false},
		{"empty rule", args{rule: nil}, "", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ruleName(tt.args.rule)
			if (err != nil) != tt.wantErr {
				t.Errorf("ruleName() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ruleName() got = %v, want %v", got, tt.want)
			}
		})
	}
}
