package html

import (
	"path/filepath"
)

// A Theme represents a certain look of the generated HTML output. All themes
// must be placed in the `templates/themes/<theme>` directory.
type Theme string

// Path returns the path of the theme relative to the html package.
func (t Theme) Path() string {
	return filepath.Join("templates/themes", string(t))
}

// The ThemePath interface must be implemented by all themes.
type ThemePath interface {
	// Path returns the path to the themes template directory.
	Path() string
}
