package go_interface

import (
	"bytes"
	"fmt"

	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/output"
)

type Generator struct {
	project *pkg.Project
}

func NewGenerator(project *pkg.Project) *Generator {
	return &Generator{project}
}

func (g *Generator) Generate() (chan *output.Artifact, chan error, chan bool) {
	out := make(chan *output.Artifact)
	errs := make(chan error)
	done := make(chan bool)

	go func() {
		out <- &output.Artifact{
			Path: "index.go",
			Data: bytes.NewBuffer([]byte(fmt.Sprintf("%+v\n", g.project))),
		}

		done <- true
	}()

	return out, errs, done
}
