package output

// The Generator converts the project and its rules to a human readable
// representation of the code style.
type Generator interface {
	// Generate will generate the actual styleguide.
	Generate() (chan *Artifact, chan error, chan bool)
}
