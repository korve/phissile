package output

import (
	"bytes"
)

// Artifact represents a file that has been generated.
type Artifact struct {
	// Path is the path relative to the current working directory where the asset
	// should be saved.
	Path string

	// Data is the actual data to write.
	Data *bytes.Buffer
}
