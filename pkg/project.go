package pkg

import (
	"fmt"
	"strings"
)

// Project is a struct that holds information about the currently analyzer
// project.
type Project struct {
	// Types is a list of code style framework that the project probably uses. Use
	// one of the PROJECT_TYPE_* constants.
	Types []ProjectType

	// ConfigFiles is a list of config files that have been found by a project
	// analyzer.
	ConfigFiles []string
}

func (p *Project) String() string {
	types := make([]string, len(p.Types))
	for i, projectType := range p.Types {
		types[i] = projectType.String()
	}

	return fmt.Sprintf("project{Types: %v, ConfigFiles: %v}", strings.Join(types, ","), p.ConfigFiles)
}
