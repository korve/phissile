package pkg

import (
	"log"
	"os"
	"path/filepath"
	"testing"

	"github.com/rs/xid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var absoluteTestDir string

func TestMain(m *testing.M) {
	dir := os.TempDir()

	if _, err := os.Stat(dir); err != nil {
		log.Fatalf("failed to read temp dir \"%s\": %s", dir, err)
	}

	id := xid.New()
	absoluteTestDir = filepath.Join(dir, id.String())

	if err := os.Mkdir(absoluteTestDir, os.ModeDir&0600); err != nil {
		log.Fatalf("failed to create temp dir \"%s\": %s", dir, err)
	}

	m.Run()

	if err := os.Remove(absoluteTestDir); err != nil {
		log.Fatalf("failed to remove temp dir \"%s\": %s", dir, err)
	}
}

func TestWalkUp(t *testing.T) {
	type args struct {
		p string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"relative path", args{"testdata/path/absolute_path"}, false},
		{"absolute path", args{absoluteTestDir}, false},
		{"not a directory", args{"testdata/path/absolute_path/file"}, true},
		{"dir does not exist", args{"testdata/path/absolute_path/doesnt_exist"}, true},
		{"empty path", args{""}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var paths []string

			if err := WalkUp(tt.args.p, func(p string) error {
				paths = append(paths, p)
				return nil
			}, false); (err != nil) != tt.wantErr {
				t.Errorf("WalkUp() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !tt.wantErr {
				for _, p := range paths {
					rel, err := filepath.Rel("/", p)

					assert.NoError(t, err)
					assert.NotContains(t, rel, "..")
				}
			}
		})
	}
}

func TestWalkUp_Symlinks(t *testing.T) {
	tmpDir := os.TempDir()
	require.DirExists(t, tmpDir)

	id := xid.New()

	symlink := filepath.Join(tmpDir, id.String())
	err := os.Symlink(absoluteTestDir, symlink)
	require.NoError(t, err)

	defer func() {
		err := os.Remove(symlink)
		require.NoError(t, err)
	}()

	var paths []string
	err = WalkUp(symlink, func(p string) error {
		paths = append(paths, p)
		return nil
	}, true)

	log.Print(paths)
	assert.NoError(t, err)

	assert.Equal(t, absoluteTestDir, paths[0])
}

func TestWalkUp_DontFollowSymlinks(t *testing.T) {
	tmpDir := os.TempDir()
	require.DirExists(t, tmpDir)

	id := xid.New()

	symlink := filepath.Join(tmpDir, id.String())
	err := os.Symlink(absoluteTestDir, symlink)
	require.NoError(t, err)

	defer func() {
		err := os.Remove(symlink)
		require.NoError(t, err)
	}()

	var paths []string
	err = WalkUp(symlink, func(p string) error {
		paths = append(paths, p)
		return nil
	}, false)

	log.Print(paths)
	assert.NoError(t, err)

	assert.NotEqual(t, absoluteTestDir, paths[0])
}
