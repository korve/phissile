package pkg

// ProjectType indicates what kind of project we are dealing with.
type ProjectType uint8

func (p *ProjectType) String() string {
	switch *p {
	case ProjectTypePhpcs:
		return "PHP_CodeSniffer"
	case ProjectTypePsalm:
		return "psalm"
	}
	return "unknown"
}

// ProjectTypePhpcs is a PHP_CodeSniffer (https://github.com/squizlabs/PHP_CodeSniffer) project
const ProjectTypePhpcs = 1

// ProjectTypePsalm indicates a psalm (https://psalm.dev/) project
const ProjectTypePsalm = 2
