package pkg

// CopyMap copies a map. It doesn't deep copy. When m is nil, nil is returned.
func CopyMap(m map[string]interface{}) map[string]interface{} {
	if m == nil {
		return nil
	}

	c := make(map[string]interface{}, len(m))
	for k, v := range m {
		c[k] = v
	}
	return c
}
