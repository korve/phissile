package pkg

import "fmt"

// Rule is a code style rule holding the following rules:
// LineLength
type Rule interface {
	String() string
}

// LineLength is a Rule indicating the maximum line length. When SoftLimit is
// reached in most frameworks a warning is printed. An error is printed when the
// HardLimit is reached.
type LineLength struct {
	SoftLimit uint64
	HardLimit uint64
}

func (l *LineLength) String() string {
	return fmt.Sprintf("line-length{SoftLimit: %d, HardLimit: %d}", l.SoftLimit, l.HardLimit)
}

// LineEnding is a Rule indicating the EOL char for each newline.
type LineEnding struct {
	EOLChar string
}

func (l *LineEnding) String() string {
	return fmt.Sprintf("line-ending{EOLChar: %s}", l.EOLChar)
}

// FileEnding is a Rule that enforces a consistent use of newlines as the last
// character of a file.
type FileEnding struct {
	// EOFNewline indicates if the file should end with a newline characters. When
	// set to true a newline character is expected as the last character.
	EOFNewline bool
}

func (f *FileEnding) String() string {
	return fmt.Sprintf("file-ending{EOFNewline: %v}", f.EOFNewline)
}

// IndentStyle is the style the project uses to indent (tab or space)
type IndentStyle int

func (i IndentStyle) String() string {
	switch i {
	case IndentStyleSpace:
		return "space"
	case IndentStyleTab:
		return "tab"
	default:
		panic(fmt.Sprintf("unknown IndentStyle %d", i))
	}
}

// IndentStyleSpace is the space indentation.
const IndentStyleSpace IndentStyle = 1

// IndentStyleTab is the tab indentation.
const IndentStyleTab IndentStyle = 2

// Indent defines whether to use spaces or tabs for indentation.
type Indent struct {
	// Style is the style (space or tab).
	Style IndentStyle

	// Size is the amount of indent characters for each indent.
	Size uint
}

func (i *Indent) String() string {
	return fmt.Sprintf("indent{style: %s, size: %d}", i.Style, i.Size)
}

// NamingSchema describes a certain style on how to name variables, classes,
// functions, constants, etc.
type NamingSchema uint8

// CamelCase fooBar
const CamelCase NamingSchema = 1

// PascalCase FooBar
const PascalCase NamingSchema = 2

// KebabCase foo-bar
const KebabCase NamingSchema = 3

// SnakeCase foo_bar
const SnakeCase NamingSchema = 4

// AllCaps FOO_BAR.
const AllCaps NamingSchema = 5

// Lowercase foobar.
const Lowercase NamingSchema = 6

// CamelCaps Foo_Bar.
const CamelCaps NamingSchema = 7

func (n NamingSchema) String() string {
	switch n {
	case CamelCase:
		return "lc-first"
	case PascalCase:
		return "uc-first"
	case KebabCase:
		return "kebab-case"
	case SnakeCase:
		return "snake-case"
	case AllCaps:
		return "all-caps"
	case Lowercase:
		return "lowercase"
	case CamelCaps:
		return "camel-caps"
	}

	panic(fmt.Sprintf("unknown naming schema %d", n))
}

// ConstantNamingSchema defines the naming style of a constant.
type ConstantNamingSchema struct {
	Schema NamingSchema
}

func (c *ConstantNamingSchema) String() string {
	return fmt.Sprintf("constant-naming-schema{style: %s}", c.Schema)
}

// ConstantValueNamingSchema defines the naming schema of a constant values (true, false, null etc.).
type ConstantValueNamingSchema struct {
	Schema NamingSchema
}

func (c *ConstantValueNamingSchema) String() string {
	return fmt.Sprintf("constant-value-naming-schema{style: %s}", c.Schema)
}

// ClassNamingSchema defines the naming schema for classes.
type ClassNamingSchema struct {
	Schema NamingSchema
}

func (c *ClassNamingSchema) String() string {
	return fmt.Sprintf("class-naming-schema{style: %s}", c.Schema)
}

// FunctionNamingSchema defines the naming schema for functions.
type FunctionNamingSchema struct {
	Schema NamingSchema
}

func (c *FunctionNamingSchema) String() string {
	return fmt.Sprintf("function-naming-schema{style: %s}", c.Schema)
}

// VariableNamingSchema defines the naming schema for variables.
type VariableNamingSchema struct {
	Schema NamingSchema
}

func (c *VariableNamingSchema) String() string {
	return fmt.Sprintf("variable-naming-schema{style: %s}", c.Schema)
}
