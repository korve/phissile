package strings

import (
	"github.com/iancoleman/strcase"
)

// ToSnakeCase converts s to snake_case.
func ToSnakeCase(s string) string {
	return strcase.ToSnake(s)
}
