package strings

import "testing"

func BenchmarkSnakeCase(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ToSnakeCase("twoWords")
	}
}

func TestSnakeCase(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"twowords", args{"twowords"}, "twowords"},
		{"TWOWORDS", args{"TWOWORDS"}, "twowords"},
		{"twoWords", args{"twoWords"}, "two_words"},
		{"TwoWords", args{"TwoWords"}, "two_words"},
		{"two_words", args{"two_words"}, "two_words"},
		{"TWO_WORDS", args{"TWO_WORDS"}, "two_words"},
		{"two_Words", args{"two_Words"}, "two_words"},
		{"Two_Words", args{"Two_Words"}, "two_words"},
		{"two-words", args{"two-words"}, "two_words"},
		{"two|words", args{"two|words"}, "two|words"},
		{"TWO-WORDS", args{"TWO-WORDS"}, "two_words"},
		{"Two-Words", args{"Two-Words"}, "two_words"},
		{"two words", args{"Two Words"}, "two_words"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ToSnakeCase(tt.args.str); got != tt.want {
				t.Errorf("SnakeCase() = %v, want %v", got, tt.want)
			}
		})
	}
}
