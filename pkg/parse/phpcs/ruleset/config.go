package ruleset

// RulePropertyElement is the array element of a property.
type RulePropertyElement struct {
	Key   string `xml:"key,attr"`
	Value string `xml:"value,attr"`
}

// RuleProperty is the <property> tag of the rule properties.
type RuleProperty struct {
	Name     string                `xml:"name,attr"`
	Value    string                `xml:"value,attr"`
	Type     string                `xml:"type,attr"`
	Elements []RulePropertyElement `xml:"element"`
}

// RuleProperties is the <properties> tag of a rule.
type RuleProperties struct {
	Properties []RuleProperty `xml:"property"`
}

// RuleExclude is the <exclude> tag of a rule.
type RuleExclude struct {
	Name string `xml:"name,attr"`
}

// Rule is the <rule> tag of a ruleset.
type Rule struct {
	Ref            string         `xml:"ref,attr"`
	Type           string         `xml:"type"`
	Excludes       []RuleExclude  `xml:"exclude"`
	Properties     RuleProperties `xml:"properties"`
	Severity       string         `xml:"severity"`
	ExcludePattern string         `xml:"exclude-pattern"`
}
