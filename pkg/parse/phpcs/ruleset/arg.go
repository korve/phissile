package ruleset

import (
	"fmt"
	"strconv"
)

// Arg is the <arg> tag of a ruleset.
type Arg struct {
	Name  string `xml:"name,attr"`
	Value string `xml:"value,attr"`
}

// IntVal get the int value of the arg. It returns an error when its unable to
// parse the args value.
func (a *Arg) IntVal() (int64, error) {
	intVal, err := strconv.ParseInt(a.Value, 10, 64)
	if err != nil {
		return 0, fmt.Errorf("failed to parse int value for arg \"%s\": %w", a.Name, err)
	}

	return intVal, nil
}
