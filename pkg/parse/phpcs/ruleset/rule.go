package ruleset

import (
	"fmt"
	"reflect"
	"strconv"
)

// An InvalidUnmarshalError describes an invalid argument passed to Unmarshal.
// (The argument to Unmarshal must be a non-nil pointer.)
type InvalidUnmarshalError struct {
	Type reflect.Type
}

func (e *InvalidUnmarshalError) Error() string {
	if e.Type == nil {
		return "json: Unmarshal(nil)"
	}

	if e.Type.Kind() != reflect.Ptr {
		return "json: Unmarshal(non-pointer " + e.Type.String() + ")"
	}
	return "json: Unmarshal(nil " + e.Type.String() + ")"
}

// PropertyNotFound will be thrown when trying to access a rule property that
// doesn't exist.
type PropertyNotFound struct {
	Name string
}

func (p *PropertyNotFound) Error() string {
	return fmt.Sprintf("property %s not found", p.Name)
}

// IsPropertyNotFoundError checks if an error is a PropertyNotFound error.
// Returns true when err is a PropertyNotFound error.
func IsPropertyNotFoundError(err error) bool {
	if _, ok := err.(*PropertyNotFound); ok {
		return true
	}
	return false
}

// UnknownRuleError is thrown when a sniff encounters a rule that it can't handle.
type UnknownRuleError struct {
	Ref string
}

func (u *UnknownRuleError) Error() string {
	return fmt.Sprintf("rule %s is not supported by this sniff", u.Ref)
}

// IsUnknownRuleError checks if an error is a UnknownRuleError error.
// Returns true when err is a UnknownRuleError error.
func IsUnknownRuleError(err error) bool {
	if _, ok := err.(*UnknownRuleError); ok {
		return true
	}
	return false
}

// UnmarshalError will be thrown when the unmarshaller is unable to unmarshal a
// property value to the target value.
type UnmarshalError struct {
	Property   string
	Value      interface{}
	Target     interface{}
	InnerError error
}

func (u *UnmarshalError) Error() string {
	return fmt.Sprintf("unable to unmarshal \"%t\" of property \"%s\" to \"%t\": %s", u.Value, u.Property, u.Target, u.InnerError)
}

// Rule implementations

func (r *Rule) String() string {
	return r.Ref
}

// HasProperty checks if the properties of a rule contain a property identified
// by name. Returns true when the property exist.
func (r *Rule) HasProperty(name string) bool {
	for _, property := range r.Properties.Properties {
		if property.Name == name {
			return true
		}
	}
	return false
}

// UnmarshalProperty tries to unmarshal a property value to v. If the property does not
// exist it will throw a PropertyNotFound error. If the property value can't be unmarshalled
// to v an UnmarshalError will be thrown.
func (r *Rule) UnmarshalProperty(name string, v interface{}) error {
	for _, property := range r.Properties.Properties {
		if property.Name == name {
			rv := reflect.ValueOf(v)

			if rv.Kind() != reflect.Ptr || rv.IsNil() {
				return &InvalidUnmarshalError{rv.Type()}
			}

			if !rv.CanAddr() {
				rv = rv.Elem()
			}

			switch rv.Kind() {
			case reflect.String:
				rv.SetString(property.Value)
				break

			case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
				intValue, err := strconv.ParseInt(property.Value, 10, 64)
				if err != nil {
					return &UnmarshalError{Property: property.Name, Value: intValue, Target: v, InnerError: err}
				}

				rv.SetInt(intValue)
				break

			case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
				strVal, err := strconv.ParseUint(property.Value, 10, 64)
				if err != nil {
					return &UnmarshalError{Property: property.Name, Value: strVal, Target: v, InnerError: err}
				}

				rv.SetUint(strVal)
				break

			case reflect.Bool:
				boolVal, err := strconv.ParseBool(property.Value)
				if err != nil {
					return &UnmarshalError{Property: property.Name, Value: boolVal, Target: v, InnerError: err}
				}
				rv.SetBool(boolVal)
				break

			default:
				return fmt.Errorf("cannot unmarshal unknown type \"%s\"", rv.Kind())
			}

			return nil
		}
	}
	return &PropertyNotFound{Name: name}
}
