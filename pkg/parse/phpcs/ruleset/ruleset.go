package ruleset

import "encoding/xml"

// Ruleset is the <ruleset> tag.
type Ruleset struct {
	XMLName                   xml.Name `xml:"ruleset"`
	Xsi                       string   `xml:"xsi,attr"`
	Name                      string   `xml:"name,attr"`
	NoNamespaceSchemaLocation string   `xml:"noNamespaceSchemaLocation,attr"`
	Description               string   `xml:"description"`
	Files                     []string `xml:"file"`
	ExcludePatterns           []string `xml:"exclude-pattern"`
	Args                      []Arg    `xml:"arg"`
	Rules                     []Rule   `xml:"rule"`

	argsMap map[string]*Arg
}

func (r *Ruleset) getArg(name string) *Arg {
	if r.argsMap == nil {
		r.argsMap = make(map[string]*Arg, len(r.Args))

		for _, arg := range r.Args {
			r.argsMap[arg.Name] = &arg
		}
	}

	return r.argsMap[name]
}

// GetArg returns an arg if it exists in Args or nil if it doesn't exist.
func (r *Ruleset) GetArg(name string) *Arg {
	return r.getArg(name)
}
