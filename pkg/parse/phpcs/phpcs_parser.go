package phpcs

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"

	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/ruleset"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/standards"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/standards/generic"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/standards/pear"
)

// Parser is responsible for parsing phpcs xml configuration files.
type Parser struct {
	s []*standards.Standard
}

func (p *Parser) ParseFile(path string) (*ruleset.Ruleset, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	data, err := ioutil.ReadAll(f)

	return p.ParseXML(data)
}

func (p *Parser) ParseXML(data []byte) (*ruleset.Ruleset, error) {
	var config *ruleset.Ruleset
	if err := xml.Unmarshal(data, &config); err != nil {
		return nil, fmt.Errorf("failed to parse XML: %w", err)
	}

	return config, nil
}

// MustParseXML parses the XML data in data and panics when the data is not a valid XML file.
func (p *Parser) MustParseXML(data []byte) *ruleset.Ruleset {
	rs, err := p.ParseXML(data)
	if err != nil {
		// the embedded ruleset should always parse
		panic(err)
	}
	return rs
}

func (p *Parser) parseRuleset(rs *ruleset.Ruleset) (chan pkg.Rule, chan error, chan bool) {
	rules := make(chan pkg.Rule)
	errs := make(chan error)
	done := make(chan bool)

	parsedRules := 0

	for _, rule := range rs.Rules {
		rule := rule

		go func() {
			for _, standard := range p.s {
				// Include whole standard respecting excludes.
				if rule.Ref == standard.Name {
					excludeMap := make(map[string]struct{}, len(rule.Excludes))
					for _, ex := range rule.Excludes {
						excludeMap[ex.Name] = struct{}{}
					}

					for sniffName, r := range standard.DefaultRules {
						if _, ok := excludeMap[sniffName]; !ok {
							rules <- r
						}
					}
					continue
				}

				for _, sniff := range standard.Sniffs {
					r, err := sniff.Parse(rs, &rule)
					if err != nil {
						if !ruleset.IsUnknownRuleError(err) {
							errs <- err
						}
						continue
					}

					if r != nil {
						rules <- r
					}
				}
			}

			parsedRules++

			if parsedRules == len(rs.Rules) {
				done <- true
			}
		}()
	}

	return rules, errs, done
}

func (p *Parser) parseRulesetSync(rs *ruleset.Ruleset) ([]pkg.Rule, error) {
	var result []pkg.Rule
	rules, errs, done := p.parseRuleset(rs)
	for {
		select {
		case r := <-rules:
			result = append(result, r)
		case err := <-errs:
			return nil, err
		case <-done:
			return result, nil
		}
	}
}

// Parse will parse a phpcs XML and return a set of rules.
func (p *Parser) Parse(project *pkg.Project) ([]pkg.Rule, error) {
	// there is only ever one configuration file used by PHPCS. So it's safe to
	// assume that the analyzer only returned the most relevant one.
	rs, err := p.ParseFile(project.ConfigFiles[0])
	if err != nil {
		return nil, fmt.Errorf("failed to parse PHPCS ruleset: %w", err)
	}

	return p.parseRulesetSync(rs)
}

// NewDefaultParser will create a Parser that has all standards registered.
func NewDefaultParser() *Parser {
	return &Parser{
		s: []*standards.Standard{
			generic.NewStandard(),
			pear.NewStandard(),
		},
	}
}
