package classes

import (
	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/standards"
)

// The ValidFunctionNameSniff maps PEAR.NamingConventions.ValidFunctionName sniffs to
// FunctionNamingSchema rules.
var ValidFunctionNameSniff = standards.NewSniffMapper(
	"PEAR.NamingConventions.ValidFunctionName",
	&pkg.FunctionNamingSchema{Schema: pkg.CamelCaps},
)
