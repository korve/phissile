package classes

import (
	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/standards"
)

// The ValidClassNameSniff maps PEAR.NamingConventions.ValidClassNameSniff sniffs to
// ClassNamingSchema rules.
var ValidClassNameSniff = standards.NewSniffMapper(
	"PEAR.NamingConventions.ValidClassName",
	&pkg.ClassNamingSchema{Schema: pkg.CamelCaps},
)
