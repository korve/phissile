package classes

import (
	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/standards"
)

// The ValidVariableNameSniff maps PEAR.NamingConventions.ValidVariableName sniffs to
// VariableNamingSchema rules.
var ValidVariableNameSniff = standards.NewSniffMapper(
	"PEAR.NamingConventions.ValidVariableName",
	&pkg.VariableNamingSchema{Schema: pkg.SnakeCase},
)
