package pear

import (
	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/standards"
	classes "codeberg.org/korve/phissile/pkg/parse/phpcs/standards/pear/naming_conventions"
)

func NewStandard() *standards.Standard {
	return &standards.Standard{
		Name: "PEAR",
		Sniffs: []standards.Sniff{
			classes.ValidClassNameSniff,
			classes.ValidFunctionNameSniff,
			classes.ValidVariableNameSniff,
		},
		DefaultRules: map[string]pkg.Rule{
			"PEAR.NamingConventions.ValidClassName":    &pkg.ClassNamingSchema{Schema: pkg.CamelCaps},
			"PEAR.NamingConventions.ValidFunctionName": &pkg.FunctionNamingSchema{Schema: pkg.CamelCaps},
			"PEAR.NamingConventions.ValidVariableName": &pkg.VariableNamingSchema{Schema: pkg.SnakeCase},
		},
	}
}
