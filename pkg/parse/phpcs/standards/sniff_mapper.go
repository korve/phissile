package standards

import (
	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/ruleset"
)

// The SniffMapper maps a phpcs rule to a phissile rule (when present).
type SniffMapper struct {
	// ref is the phpcs rule name to map from.
	ref string
	// to is the target rule to map.
	to pkg.Rule
}

// Parse parses rule and checks if the ref matches. It returns the to rule when
// ref matches or UnknownRuleError if not.
func (p *SniffMapper) Parse(_ *ruleset.Ruleset, rule *ruleset.Rule) (pkg.Rule, error) {
	if rule.Ref == p.ref {
		return p.to, nil
	}
	return nil, &ruleset.UnknownRuleError{Ref: rule.Ref}
}

// NewSniffMapper creates a new SniffMapper instance.
func NewSniffMapper(ref string, to pkg.Rule) *SniffMapper {
	return &SniffMapper{
		ref: ref,
		to:  to,
	}
}
