package standards

import (
	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/ruleset"
)

// Sniff represents a PHPCS code sniff. For example:
// https://github.com/squizlabs/PHP_CodeSniffer/blob/master/src/Standards/Generic/Sniffs/Files/LineEndingsSniff.php
// A sniff in phissile can parse a rule from the phpcs XML file and return a
// phissile rule.
type Sniff interface {

	// Parse parses a phpcs XML rule and turns it into a phissile rule. When the
	// rule is unknown to the sniff it should throw a UnknownRuleError error.
	Parse(rs *ruleset.Ruleset, rule *ruleset.Rule) (pkg.Rule, error)
}
