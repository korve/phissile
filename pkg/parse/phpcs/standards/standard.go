package standards

import (
	"codeberg.org/korve/phissile/pkg"
)

// Standard represents a PHPCS standard. Standards in PHPCS are kind of like
// templates. They contain a set of Sniffs and a phpcs.xml. See:
// https://github.com/squizlabs/PHP_CodeSniffer/tree/master/src/Standards
type Standard struct {
	// The name of the standard. Its the same name as the name in the PHPCS
	// standards directory.
	Name string

	// A list of sniffs this standard provides.
	Sniffs []Sniff

	// Ruleset is a path to a ruleset configuration .xml that is parsed
	// when the standard is included completely.
	DefaultRules map[string]pkg.Rule
}
