package files

import (
	"fmt"

	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/ruleset"
)

// LineLengthSniff parses Generic.Files.LineLength sniffs and returns a pkg.LineLength rule.
type LineLengthSniff struct{}

// Parse checks if rule is a Generic.Files.LineLength sniff and returns soft/hard limits according to
// the sniffs properties.
func (l *LineLengthSniff) Parse(_ *ruleset.Ruleset, rule *ruleset.Rule) (pkg.Rule, error) {
	if rule.Ref == "Generic.Files.LineLength" {
		var softLimit uint64
		var hardLimit uint64

		if rule.HasProperty("lineLimit") {
			err := rule.UnmarshalProperty("lineLimit", &softLimit)
			if err != nil {
				return nil, fmt.Errorf("failed to parse lineLimit: %w", err)
			}
		}
		if rule.HasProperty("absoluteLineLimit") {
			err := rule.UnmarshalProperty("absoluteLineLimit", &hardLimit)
			if err != nil {
				return nil, fmt.Errorf("failed to parse absoluteLineLimit: %w", err)
			}
		}

		if softLimit <= 0 {
			softLimit = 80
		}

		if hardLimit <= 0 {
			hardLimit = 100
		}

		if softLimit > hardLimit {
			softLimit = hardLimit
		}

		return &pkg.LineLength{
			SoftLimit: softLimit,
			HardLimit: hardLimit,
		}, nil
	}
	return nil, &ruleset.UnknownRuleError{Ref: rule.Ref}
}
