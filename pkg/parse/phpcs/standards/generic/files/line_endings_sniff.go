package files

import (
	"fmt"

	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/ruleset"
)

// LineEndingsSniff parses Generic.Files.LineLength sniffs and returns a
// pkg.LineEnding rule.
type LineEndingsSniff struct{}

// Parse parses the Generic.Files.EndFileNoNewlineSniff and returns a
// pkg.LineEnding rule.
func (l *LineEndingsSniff) Parse(_ *ruleset.Ruleset, rule *ruleset.Rule) (pkg.Rule, error) {
	if rule.Ref == "Generic.Files.LineEndings" {
		var lf string
		if rule.HasProperty("eolChar") {
			err := rule.UnmarshalProperty("eolChar", &lf)
			if err != nil {
				return nil, err
			}
			if lf == "" {
				return nil, fmt.Errorf("eolChar property is empty")
			}

			if lf != "\\n" && lf != "\\r\\n" {
				return nil, fmt.Errorf("invalid eolChar \"%s\"", lf)
			}

		} else {
			lf = "\\n"
		}

		return &pkg.LineEnding{EOLChar: lf}, nil
	}
	return nil, &ruleset.UnknownRuleError{Ref: rule.Ref}
}
