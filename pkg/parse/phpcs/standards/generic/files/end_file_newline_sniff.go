package files

import (
	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/standards"
)

// EndFileNewlineSniff parses Generic.Files.EndFileNewlineSniff sniffs and
// returns a pkg.FileEnding which expects a newline at the end of the file.
var EndFileNewlineSniff = standards.NewSniffMapper(
	"Generic.Files.EndFileNewlineSniff",
	&pkg.FileEnding{EOFNewline: true},
)
