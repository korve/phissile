package files

import (
	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/standards"
)

// EndFileNoNewlineSniff parses Generic.Files.EndFileNoNewlineSniff sniffs and
// returns a pkg.FileEnding which expects NO newline at the end of the file.
var EndFileNoNewlineSniff = standards.NewSniffMapper(
	"Generic.Files.EndFileNoNewlineSniff",
	&pkg.FileEnding{EOFNewline: false},
)
