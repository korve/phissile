package files

import (
	"fmt"
	"reflect"
	"testing"

	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/ruleset"
)

func newLineLengthRule(softLimit int, hardLimit int) *ruleset.Rule {
	return &ruleset.Rule{
		Ref: "Generic.Files.LineLength",
		Properties: ruleset.RuleProperties{
			Properties: []ruleset.RuleProperty{
				{Name: "lineLimit", Value: fmt.Sprintf("%d", softLimit)},
				{Name: "absoluteLineLimit", Value: fmt.Sprintf("%d", hardLimit)},
			},
		},
	}
}

func TestLineLengthSniff_Parse(t *testing.T) {
	type args struct {
		rule *ruleset.Rule
	}
	tests := []struct {
		name    string
		args    args
		want    pkg.Rule
		wantErr bool
	}{
		{"soft and hard limit", args{newLineLengthRule(50, 100)}, &pkg.LineLength{SoftLimit: 50, HardLimit: 100}, false},
		{"no soft limit", args{newLineLengthRule(0, 100)}, &pkg.LineLength{SoftLimit: 80, HardLimit: 100}, false},
		{"no hard limit", args{newLineLengthRule(50, 0)}, &pkg.LineLength{SoftLimit: 50, HardLimit: 100}, false},
		{"softLimit gt hardLimit", args{newLineLengthRule(500, 100)}, &pkg.LineLength{SoftLimit: 100, HardLimit: 100}, false},
		{"negative soft limit", args{newLineLengthRule(-5, 100)}, nil, true},
		{"negative hard limit", args{newLineLengthRule(-5, 100)}, nil, true},
		{"unknown rule", args{&ruleset.Rule{Ref: "Foobar"}}, nil, true},
		{"defaults", args{&ruleset.Rule{Ref: "Generic.Files.LineLength"}}, &pkg.LineLength{SoftLimit: 80, HardLimit: 100}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := &LineLengthSniff{}
			got, err := l.Parse(&ruleset.Ruleset{}, tt.args.rule)
			if (err != nil) != tt.wantErr {
				t.Errorf("Parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Parse() got = %v, want %v", got, tt.want)
			}
		})
	}
}
