package files

import (
	"reflect"
	"testing"

	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/ruleset"
)

func newLineEndingRule(eolChar string) *ruleset.Rule {
	return &ruleset.Rule{
		Ref: "Generic.Files.LineEndings",
		Properties: ruleset.RuleProperties{
			Properties: []ruleset.RuleProperty{
				{Name: "eolChar", Value: eolChar},
			},
		},
	}
}

func TestLineEndingsSniff_Parse(t *testing.T) {
	type args struct {
		rule *ruleset.Rule
	}
	tests := []struct {
		name    string
		args    args
		want    pkg.Rule
		wantErr bool
	}{
		{"linux eol", args{newLineEndingRule("\\n")}, &pkg.LineEnding{EOLChar: "\\n"}, false},
		{"windows eol", args{newLineEndingRule("\\r\\n")}, &pkg.LineEnding{EOLChar: "\\r\\n"}, false},
		{"empty", args{newLineEndingRule("")}, nil, true},
		{"default", args{&ruleset.Rule{Ref: "Generic.Files.LineEndings"}}, &pkg.LineEnding{EOLChar: "\\n"}, false},
		{"default", args{&ruleset.Rule{Ref: "Generic.Files.LineEndings"}}, &pkg.LineEnding{EOLChar: "\\n"}, false},
		{"unknown", args{&ruleset.Rule{Ref: "Foobar"}}, nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := &LineEndingsSniff{}
			got, err := l.Parse(&ruleset.Ruleset{}, tt.args.rule)
			if (err != nil) != tt.wantErr {
				t.Errorf("Parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Parse() got = %v, want %v", got, tt.want)
			}
		})
	}
}
