package php

import (
	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/standards"
)

// The LowerCaseConstantSniff maps Generic.PHP.LowerCaseConstantSniff sniffs to
// ConstantValueNamingSchema rules.
var LowerCaseConstantSniff = standards.NewSniffMapper(
	"Generic.PHP.LowerCaseConstantSniff",
	&pkg.ConstantValueNamingSchema{
		Schema: pkg.Lowercase,
	},
)
