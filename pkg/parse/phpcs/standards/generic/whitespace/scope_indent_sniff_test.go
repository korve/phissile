package whitespace

import (
	"fmt"
	"reflect"
	"testing"

	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/ruleset"
)

func newIndentRule(tabIndent bool, indent int) *ruleset.Rule {
	return &ruleset.Rule{
		Ref: "Generic.WhiteSpace.ScopeIndent",
		Properties: ruleset.RuleProperties{
			Properties: []ruleset.RuleProperty{
				{Name: "indent", Value: fmt.Sprintf("%d", indent)},
				{Name: "tabIndent", Value: fmt.Sprintf("%v", tabIndent)},
			},
		},
	}
}

func TestScopeIndentSniff_Parse(t *testing.T) {
	type args struct {
		rule *ruleset.Rule
	}
	tests := []struct {
		name    string
		args    args
		want    pkg.Rule
		wantErr bool
	}{
		{"tabs", args{newIndentRule(true, 0)}, &pkg.Indent{Size: 1, Style: pkg.IndentStyleTab}, false},
		{"spaces", args{newIndentRule(false, 2)}, &pkg.Indent{Size: 2, Style: pkg.IndentStyleSpace}, false},
		{"only size", args{&ruleset.Rule{
			Ref: "Generic.WhiteSpace.ScopeIndent",
			Properties: ruleset.RuleProperties{
				Properties: []ruleset.RuleProperty{
					{Name: "indent", Value: fmt.Sprintf("%d", 2)},
				},
			},
		}}, &pkg.Indent{Size: 2, Style: pkg.IndentStyleSpace}, false},
		{"only tabIndent", args{&ruleset.Rule{
			Ref: "Generic.WhiteSpace.ScopeIndent",
			Properties: ruleset.RuleProperties{
				Properties: []ruleset.RuleProperty{
					{Name: "tabIndent", Value: fmt.Sprintf("%v", true)},
				},
			},
		}}, &pkg.Indent{Size: 1, Style: pkg.IndentStyleTab}, false},
		{"defaults", args{&ruleset.Rule{Ref: "Generic.WhiteSpace.ScopeIndent"}}, &pkg.Indent{Size: 4, Style: pkg.IndentStyleSpace}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := &ScopeIndentSniff{}
			got, err := l.Parse(&ruleset.Ruleset{}, tt.args.rule)
			if (err != nil) != tt.wantErr {
				t.Errorf("Parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Parse() got = %v, want %v", got, tt.want)
			}
		})
	}
}
