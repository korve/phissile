package whitespace

import (
	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/ruleset"
)

// ScopeIndentSniff parses the Generic.WhiteSpace.ScopeIndent sniff.
type ScopeIndentSniff struct {
}

// Parse parses the Generic.WhiteSpace.ScopeIndent and returns a pkg.Indent
// rule.
func (s *ScopeIndentSniff) Parse(_ *ruleset.Ruleset, rule *ruleset.Rule) (pkg.Rule, error) {
	if rule.Ref == "Generic.WhiteSpace.ScopeIndent" {
		tabIndent := false
		var size uint = 4
		if err := rule.UnmarshalProperty("tabIndent", &tabIndent); err != nil && !ruleset.IsPropertyNotFoundError(err) {
			return nil, err
		}

		if tabIndent == false {
			if err := rule.UnmarshalProperty("indent", &size); err != nil && !ruleset.IsPropertyNotFoundError(err) {
				return nil, err
			}
		} else {
			size = 1
		}

		r := &pkg.Indent{
			Size: size,
		}

		if tabIndent == true {
			r.Style = pkg.IndentStyleTab
		} else {
			r.Style = pkg.IndentStyleSpace
		}

		return r, nil
	}
	return nil, &ruleset.UnknownRuleError{Ref: rule.Ref}
}
