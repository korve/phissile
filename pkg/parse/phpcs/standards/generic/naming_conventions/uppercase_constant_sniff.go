package naming_conventions

import (
	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/standards"
)

// The UpperCaseConstantName returns a AllCaps ConstantNamingSchema when a
// `Generic.NamingConventions.UpperCaseConstantName` sniff is present.
var UpperCaseConstantName = standards.NewSniffMapper(
	"Generic.NamingConventions.UpperCaseConstantName",
	&pkg.ConstantNamingSchema{
		Schema: pkg.AllCaps,
	},
)
