package generic

import (
	"codeberg.org/korve/phissile/pkg/parse/phpcs/standards"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/standards/generic/files"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/standards/generic/naming_conventions"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/standards/generic/php"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/standards/generic/whitespace"
)

// NewStandard defines the
// https://github.com/squizlabs/PHP_CodeSniffer/tree/master/src/Standards/Generic
// standard.
func NewStandard() *standards.Standard {
	return &standards.Standard{
		Name: "Generic",
		Sniffs: []standards.Sniff{
			// Generic.Files
			&files.LineEndingsSniff{},
			&files.LineLengthSniff{},
			files.EndFileNewlineSniff,
			files.EndFileNoNewlineSniff,

			// Generic.Whitespace
			&whitespace.ScopeIndentSniff{},

			// Generic.NamingConventions
			naming_conventions.UpperCaseConstantName,

			// Generic.PHP
			php.LowerCaseConstantSniff,
		},
	}
}
