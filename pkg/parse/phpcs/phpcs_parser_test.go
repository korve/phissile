package phpcs

import (
	"encoding/xml"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"codeberg.org/korve/phissile/pkg"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/ruleset"
	"codeberg.org/korve/phissile/pkg/parse/phpcs/standards/pear"
)

func Test_parseFile(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name    string
		args    args
		want    *ruleset.Ruleset
		wantErr bool
	}{
		{"simple file", args{"testdata/phpcs_simple.xml"}, &ruleset.Ruleset{
			XMLName:                   xml.Name{Local: "ruleset"},
			Xsi:                       "http://www.w3.org/2001/XMLSchema-instance",
			Name:                      "PHP_CodeSniffer",
			NoNamespaceSchemaLocation: "phpcs.xsd",
			Description:               "Simple File",
			Files:                     []string{"autoload.php"},
			ExcludePatterns:           []string{"*/foo/bar"},
			Args:                      []ruleset.Arg{{Name: "basepath", Value: "."}},
			Rules:                     []ruleset.Rule{{Ref: "Squiz.Arrays.ArrayBracketSpacing"}},
		}, false},
		{"complete file", args{"testdata/phpcs.xml"}, &ruleset.Ruleset{
			XMLName:                   xml.Name{Local: "ruleset"},
			Xsi:                       "http://www.w3.org/2001/XMLSchema-instance",
			Name:                      "PHP_CodeSniffer",
			NoNamespaceSchemaLocation: "phpcs.xsd",
			Description:               "The coding standard for PHP_CodeSniffer itself.",
			Files:                     []string{"autoload.php", "tests"},
			ExcludePatterns: []string{
				"*/src/Standards/*/Tests/*\\.(inc|css|js)$",
				"*/tests/Core/*/*\\.(inc|css|js)$",
			},
			Args: []ruleset.Arg{
				{Name: "basepath", Value: "."},
				{Name: "colors"},
			},
			Rules: []ruleset.Rule{
				{Ref: "Internal.Tokenizer.Exception", Type: "error"},
				{Ref: "PEAR", Excludes: []ruleset.RuleExclude{
					{Name: "PEAR.NamingConventions.ValidFunctionName"},
					{Name: "PEAR.NamingConventions.ValidVariableName"},
				}},
				{Ref: "Squiz.Arrays.ArrayDeclaration.KeyNotAligned", Severity: "0"},
				{Ref: "Generic.Formatting.MultipleStatementAlignment", Properties: ruleset.RuleProperties{
					Properties: []ruleset.RuleProperty{
						{Name: "maxPadding", Value: "12"},
						{Name: "error", Value: "true"},
					},
				}},
				{Ref: "Generic.PHP.ForbiddenFunctions", Properties: ruleset.RuleProperties{
					Properties: []ruleset.RuleProperty{
						{Name: "forbiddenFunctions", Type: "array", Elements: []ruleset.RulePropertyElement{
							{Key: "sizeof", Value: "count"},
							{Key: "delete", Value: "unset"},
						}},
					},
				}},
				{Ref: "Generic.Strings.UnnecessaryStringConcat", ExcludePattern: "tests/bootstrap\\.php"},
			},
		}, false},
		{"include standard", args{"testdata/include.xml"}, &ruleset.Ruleset{
			XMLName:                   xml.Name{Local: "ruleset"},
			Xsi:                       "http://www.w3.org/2001/XMLSchema-instance",
			Name:                      "PHP_CodeSniffer",
			NoNamespaceSchemaLocation: "phpcs.xsd",
			Rules: []ruleset.Rule{
				{Ref: "PEAR"},
			},
		}, false},
		{"include standard with excludes", args{"testdata/include_exclude.xml"}, &ruleset.Ruleset{
			XMLName:                   xml.Name{Local: "ruleset"},
			Xsi:                       "http://www.w3.org/2001/XMLSchema-instance",
			Name:                      "PHP_CodeSniffer",
			NoNamespaceSchemaLocation: "phpcs.xsd",
			Rules: []ruleset.Rule{
				{Ref: "PEAR", Excludes: []ruleset.RuleExclude{
					{Name: "PEAR.NamingConventions.ValidClassNameSniff"},
				}},
			},
		}, false},
		{"invalid file", args{"testdata/invalid.xml"}, nil, true},
		{"missing file", args{"testdata/missing.xml"}, nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Parser{}
			got, err := p.ParseFile(tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseFile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.EqualValues(t, tt.want, got)
		})
	}
}

func TestParser_Parse(t *testing.T) {
	pearStandard := pear.NewStandard()
	pearRules := make([]pkg.Rule, len(pearStandard.DefaultRules))
	for _, rule := range pearStandard.DefaultRules {
		pearRules = append(pearRules, rule)
	}

	type args struct {
		path string
	}
	tests := []struct {
		name    string
		args    args
		want    []pkg.Rule
		wantErr bool
	}{
		{"simple file", args{"testdata/basic.xml"}, []pkg.Rule{
			&pkg.LineEnding{EOLChar: "\\r\\n"},
			&pkg.LineLength{SoftLimit: 100, HardLimit: 200},
		}, false},
		{"include", args{"testdata/include.xml"}, pearRules, false},
		{"file does not exist", args{"testdata/does_not_exist.xml"}, nil, true},
		{"non existent rule", args{"testdata/non_existent_rule.xml"}, []pkg.Rule{}, false},
		{"invalid rule", args{"testdata/rules/line_length_negative.xml"}, nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewDefaultParser()

			got, err := p.Parse(&pkg.Project{
				Types:       []pkg.ProjectType{pkg.ProjectTypePhpcs},
				ConfigFiles: []string{tt.args.path},
			})

			if tt.wantErr {
				assert.Error(t, err)
				assert.Nil(t, got)
			} else {
				assert.NoError(t, err)
				assert.Equal(t, tt.want, got)
			}
		})
	}
}

func TestParser_Parse_IncludeWithExclusion(t *testing.T) {
	p := NewDefaultParser()
	rules, err := p.Parse(&pkg.Project{
		Types:       []pkg.ProjectType{pkg.ProjectTypePhpcs},
		ConfigFiles: []string{"testdata/include_exclude.xml"},
	})

	require.NotNil(t, rules)

	for _, rule := range rules {
		if _, ok := rule.(*pkg.ClassNamingSchema); ok {
			assert.Fail(t, "expected ClassNamingSchema to be excluded")
		}
	}

	assert.NoError(t, err)
}

func TestParser_Parse_MappedRules(t *testing.T) {
	p := NewDefaultParser()

	got, err := p.Parse(&pkg.Project{
		Types:       []pkg.ProjectType{pkg.ProjectTypePhpcs},
		ConfigFiles: []string{"testdata/mapped.xml"},
	})

	require.NoError(t, err)

	assert.Equal(t, []pkg.Rule{
		// Generic.PHP.LowerCaseConstantSniff
		&pkg.ConstantValueNamingSchema{Schema: pkg.Lowercase},
		// Generic.NamingConventions.UpperCaseConstantName
		&pkg.ConstantNamingSchema{Schema: pkg.AllCaps},
		// Generic.Files.EndFileNewlineSniff
		&pkg.FileEnding{EOFNewline: true},
		// Generic.Files.EndFileNoNewlineSniff
		&pkg.FileEnding{EOFNewline: false},
	}, got)
}
