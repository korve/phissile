package pkg

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCopyMap(t *testing.T) {
	type args struct {
		m map[string]interface{}
	}
	fn := func() {}
	tests := []struct {
		name string
		args args
		want map[string]interface{}
	}{
		{"nil map", args{nil}, nil},
		{"empty map", args{map[string]interface{}{}}, map[string]interface{}{}},
		{"filled map", args{map[string]interface{}{"foo": "bar"}}, map[string]interface{}{"foo": "bar"}},
		{"fn map", args{map[string]interface{}{"foo": &fn}}, map[string]interface{}{"foo": &fn}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := CopyMap(tt.args.m)
			assert.Equal(t, tt.want, got)
		})
	}
}
