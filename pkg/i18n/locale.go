package i18n

import (
	"fmt"
	"regexp"
)

// Locale represents a locale defined by the first two segments
// ([language[_territory]) of the ISO/IEC 15897 standard.
type Locale string

// DefaultLocale is used when no other language is provided by the user.
const DefaultLocale Locale = "en-EN"

var localeRegex = regexp.MustCompile("^[a-z]{2}(_[A-Z]{2})?$")

// IsValid checks if locale matches the first two segments of the ISO/IEC 15897
// ([language[_territory], e.g. de_DE) standard.
func IsValid(locale string) bool {
	return localeRegex.Match([]byte(locale))
}

// Parse parses a string to a locale. If the string does not match the first two
// segments of the ISO/IEC 15897 ([language[_territory], e.g. de_DE) Parse will
// return an error.
func Parse(val string) (Locale, error) {
	if !IsValid(val) {
		return "", fmt.Errorf("%s is not va valid val", val)
	}

	return Locale(val), nil
}
