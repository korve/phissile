package i18n

import (
	"embed"
	"encoding/json"
	"errors"
	"os"
	"strings"
	"text/template"
)

//go:embed translations/*.json
var embedFs embed.FS

var loadedTranslations = make(map[Locale]*map[string]string, 1)
var parsedTemplates = make(map[string]*template.Template)

func loadTranslation(locale Locale) error {
	if _, ok := loadedTranslations[locale]; ok {
		return nil
	}

	data, err := embedFs.ReadFile("translations/" + string(locale[:2]) + ".json")
	if err != nil && !os.IsNotExist(err) {
		return err
	}

	// try locale without suffix
	if err != nil {
		data, err = embedFs.ReadFile("translations/" + string(locale) + ".json")
	}

	if err != nil {
		if os.IsNotExist(err) {
			return ErrUnsupportedLocale
		}

		return err
	}

	var translations *map[string]string
	if err := json.Unmarshal(data, &translations); err != nil {
		return err
	}

	loadedTranslations[locale] = translations
	return nil
}

func renderTranslation(key string, value string, params map[string]string) (string, error) {
	var err error

	t, exists := parsedTemplates[key]
	if !exists {
		// parse template
		t, err = template.New(key).Parse(value)
		if err != nil {
			return "", err
		}
	}

	s := strings.Builder{}

	if err := t.Execute(&s, params); err != nil {
		return "", err
	}

	return s.String(), nil
}

// Trans translates text to a localized version. The key identifies the value to
// translate and is the key in the translations JSON file. The locale determines
// which translation file to use and params is an optional map that will be
// provided to the template.Execute method when rendering the final translation
// value. You can use go template strings in the translation json file.
func Trans(key string, locale Locale, params map[string]string) (string, error) {
	err := loadTranslation(locale)
	if err != nil {
		if errors.Is(err, ErrUnsupportedLocale) {
			return "", &TranslateErr{Key: key, Locale: locale, Err: err}
		}

		// otherwise we were not able to load the embedded file system which shouldn't
		// happen.
		panic(err)
	}

	value, ok := (*loadedTranslations[locale])[key]
	if !ok {
		return "", &TranslateErr{Key: key, Locale: locale, Err: ErrNotFound}
	}

	if strings.Contains(value, "{") {
		rendered, err := renderTranslation(key, value, params)
		if err != nil {
			return "", &TranslateErr{Key: key, Locale: locale, Err: err}
		}
		return rendered, nil
	}

	return value, nil
}

// RegisterTranslations manually registers translations for the provided locale.
func RegisterTranslations(translations *map[string]string, locale Locale) {
	loadedTranslations[locale] = translations
}
