package i18n

import (
	"errors"
	"fmt"
	"testing"
)

func TestIsErrInvalid(t *testing.T) {
	type args struct {
		t *TranslateErr
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"is ErrInvalid", args{&TranslateErr{Err: ErrInvalid}}, true},
		{"is not ErrInvalid", args{&TranslateErr{Err: errors.New("foo")}}, false},
		{"nil", args{&TranslateErr{Err: nil}}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsErrInvalid(tt.args.t); got != tt.want {
				t.Errorf("IsErrInvalid() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsErrNotFound(t *testing.T) {
	type args struct {
		t *TranslateErr
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"is ErrNotFound", args{&TranslateErr{Err: ErrNotFound}}, true},
		{"is not ErrNotFound", args{&TranslateErr{Err: errors.New("foo")}}, false},
		{"nil", args{&TranslateErr{Err: nil}}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsErrNotFound(tt.args.t); got != tt.want {
				t.Errorf("IsErrNotFound() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsErrUnsupportedLocale(t *testing.T) {
	type args struct {
		t *TranslateErr
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"is ErrUnsupportedLocale", args{&TranslateErr{Err: ErrUnsupportedLocale}}, true},
		{"is not ErrUnsupportedLocale", args{&TranslateErr{Err: errors.New("foo")}}, false},
		{"nil", args{&TranslateErr{Err: nil}}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsErrUnsupportedLocale(tt.args.t); got != tt.want {
				t.Errorf("IsErrUnsupportedLocale() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTranslateErr_Error(t1 *testing.T) {
	type fields struct {
		Key    string
		Locale Locale
		Err    error
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{"", fields{
			Key:    "foo",
			Locale: "xx_XX",
			Err:    ErrInvalid,
		}, fmt.Sprintf("failed to translate \"foo\" (xx_XX): %v", ErrInvalid)},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := &TranslateErr{
				Key:    tt.fields.Key,
				Locale: tt.fields.Locale,
				Err:    tt.fields.Err,
			}
			if got := t.Error(); got != tt.want {
				t1.Errorf("Error() = %v, want %v", got, tt.want)
			}
		})
	}
}
