package i18n

import (
	"errors"
	"fmt"
)

// TranslateErr occurs when Trans fails translating a value. This can be the case
// when the locale doesn't exist, the translation key doesn't exist or the template
// string couldn't be rendered.
type TranslateErr struct {
	// Key is the key that Trans was called with.
	Key string

	// Locale is the locale that Trans was called with.
	Locale Locale

	// Err is the inner error that occurred. Can be ErrUnsupportedLocale,
	// ErrNotFound, ErrInvalid or any other error that can occur when
	// opening/parsing the translations.
	Err error
}

func (t *TranslateErr) Error() string {
	return fmt.Sprintf("failed to translate \"%s\" (%s): %s", t.Key, t.Locale, t.Err)
}

// IsErrInvalid returns whether the error is a ErrInvalid error.
func IsErrInvalid(t *TranslateErr) bool {
	return errors.Is(t.Err, ErrInvalid)
}

// IsErrNotFound returns whether the error is a ErrNotFound error.
func IsErrNotFound(t *TranslateErr) bool {
	return errors.Is(t.Err, ErrNotFound)
}

// IsErrUnsupportedLocale returns whether the error is a ErrUnsupportedLocale
// error.
func IsErrUnsupportedLocale(t *TranslateErr) bool {
	return errors.Is(t.Err, ErrUnsupportedLocale)
}

var (
	// ErrUnsupportedLocale is thrown when the requested locale doesn't exist.
	ErrUnsupportedLocale = errors.New("locale is unsupported")

	// ErrNotFound is thrown when the requested locale doesn't exist.
	ErrNotFound = errors.New("translation not found")

	// ErrInvalid is thrown when a template string is invalid.
	ErrInvalid = errors.New("the translation is invalid")
)
