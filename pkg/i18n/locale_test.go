package i18n

import "testing"

func TestIsValid(t *testing.T) {
	type args struct {
		locale string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"de_DE", args{"de_DE"}, true},
		{"de", args{"de"}, true},
		{"en_", args{"en_"}, false},
		{"_US", args{"_US"}, false},
		{"DE_DE", args{"DE_DE"}, false},
		{"de_de", args{"de_de"}, false},
		{"de_de.UTF-8", args{"de_de.UTF-8"}, false},
		{"empty", args{""}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsValid(tt.args.locale); got != tt.want {
				t.Errorf("IsValid() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParse(t *testing.T) {
	type args struct {
		val string
	}
	tests := []struct {
		name    string
		args    args
		want    Locale
		wantErr bool
	}{
		{"de_DE", args{"de_DE"}, Locale("de_DE"), false},
		{"de", args{"de"}, Locale("de"), false},
		{"empty", args{""}, Locale(""), true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Parse(tt.args.val)
			if (err != nil) != tt.wantErr {
				t.Errorf("Parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Parse() got = %v, want %v", got, tt.want)
			}
		})
	}
}
