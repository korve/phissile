package i18n

import "testing"

var testLocale Locale = "xx_XX"

func TestMain(m *testing.M) {
	loadedTranslations = map[Locale]*map[string]string{
		testLocale: {
			"simple":      "foo bar",
			"hello":       "hello {{.planet}}",
			"invalid":     "Broken {{}",
			"":            "empty key",
			"empty-value": "empty value",
		},
	}

	m.Run()
}

func TestTrans(t *testing.T) {
	type args struct {
		key    string
		locale Locale
		params map[string]string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{"simple", args{key: "simple", locale: testLocale, params: map[string]string{}}, "foo bar", false},
		{"with template", args{key: "hello", locale: testLocale, params: map[string]string{"planet": "world"}}, "hello world", false},
		{"empty key", args{key: "", locale: testLocale, params: map[string]string{}}, "empty key", false},
		{"empty value", args{key: "empty-value", locale: testLocale, params: map[string]string{}}, "empty value", false},
		{"non existing", args{key: "non-existing", locale: testLocale, params: map[string]string{}}, "", true},
		{"invalid template", args{key: "invalid", locale: testLocale, params: map[string]string{}}, "", true},
		{"unsupported locale", args{key: "simple", locale: "zz_ZZ", params: map[string]string{}}, "", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Trans(tt.args.key, tt.args.locale, tt.args.params)
			if (err != nil) != tt.wantErr {
				t.Errorf("Trans() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Trans() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_loadTranslation(t *testing.T) {
	type args struct {
		locale Locale
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"en", args{"en"}, false},
		{"en_US", args{"en_US"}, false},
		{"zz_ZZ", args{"zz_ZZ"}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := loadTranslation(tt.args.locale); (err != nil) != tt.wantErr {
				t.Errorf("loadTranslation() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
