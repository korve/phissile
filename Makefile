.PHONY: build test lint help

all: lint test build

build: ## build the phissile binary
	go build -o build/phissile ./main.go

test: ## run all tests
	go test ./...

lint: ## run golint
	golint ./...

help: ## display this help message
	@echo "Please use \`make <target>' where <target> is one of"
	@perl -nle'print $& if m{^[a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m  %-25s\033[0m %s\n", $$1, $$2}'