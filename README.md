# Phissile

Phissile lets you generate human readable code style documents from your php linter configs (phpcs, psalm).

### Usage

```
phissile [command]

Available Commands:
  generate    Generates a styleguide from a lint config
  help        Help about any command

Flags:
  -h, --help   help for phissile

Use "phissile [command] --help" for more information about a command.
```

### Building

In order to build phissile you need at least go 1.16. Run `make build`
to compile the phissile binary. The binary will be compiled to the build folder.

### Testing

Run `make test` to run all tests.

### Lint

Run `make lint` to lint the code. [golint](https://github.com/golang/lint) is required

### License

GPL-3.0-only