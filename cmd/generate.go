package cmd

import (
	"context"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"

	"codeberg.org/korve/phissile/pkg/analyze"
	output2 "codeberg.org/korve/phissile/pkg/output"
	"codeberg.org/korve/phissile/pkg/output/go_interface"
	"codeberg.org/korve/phissile/pkg/output/html"
	"codeberg.org/korve/phissile/pkg/parse/phpcs"
)

var verbose bool
var outputDir string
var outputType string

// The OutputType defines how the output will be formatted.
type OutputType string

// OutputTypeHtml generates a index.html with the styleguide.
const OutputTypeHtml OutputType = "html"

// OutputTypeGoInterface is an internal helper to generate the rules as interface definitions.
const OutputTypeGoInterface OutputType = "__go_interface"

var allOutputTypes = []string{
	string(OutputTypeHtml),
	string(OutputTypeGoInterface),
}

// generateCmd represents the generate command
var generateCmd = &cobra.Command{
	Use:   "generate",
	Short: "Generates a styleguide from a lint config",
	Run: func(cmd *cobra.Command, args []string) {
		var dir string
		if len(args) == 0 {
			wd, err := os.Getwd()
			if err != nil {
				panic(err)
			}
			dir = wd
		} else {
			dir = args[0]
		}

		if verbose {
			log.Printf("analyzing project at \"%s\"...\n", dir)
		}

		analyzer := analyze.NewAggregateProjectAnalyzer([]analyze.ProjectAnalyzer{
			&analyze.PHPCSAnalyzer{},
		})

		project, err := analyzer.Analyze(context.Background(), dir)
		if err != nil {
			log.Fatalf("generate failed: %s", err)
		}

		if len(project.ConfigFiles) == 0 {
			log.Fatalf("no projects found at %s", dir)
		}

		if verbose {
			log.Printf("found project: %s\n", project)
		}

		parser := phpcs.NewDefaultParser()
		rules, err := parser.Parse(project)
		if err != nil {
			log.Fatalf("failed to parse project: %s", err)
		}

		if verbose {
			log.Println(fmt.Sprintf("found %d rules: ", len(rules)))

			for _, rule := range rules {
				log.Println(fmt.Sprintf("\t %s", rule))
			}
		}

		var g output2.Generator

		switch OutputType(outputType) {
		case OutputTypeHtml:
			g = html.NewGenerator(
				project,
				rules,
				html.DefaultTheme,
			)
		case OutputTypeGoInterface:
			g = go_interface.NewGenerator(project)
		default:
			log.Fatalf("invalid output type: %s", outputType)
		}

		output, errs, done := g.Generate()

		var files []string

	Generate:
		for {
			select {
			case o := <-output:
				p := filepath.Join(outputDir, o.Path)

				f, err := os.Create(p)
				if err != nil {
					log.Fatalf("failed to open file %s: %s", o.Path, err)
				}

				if _, err = o.Data.WriteTo(f); err != nil {
					if err := f.Close(); err != nil {
						panic(err)
					}
					log.Fatalf("failed to close file %s: %s", o.Path, err)
				}

				files = append(files, o.Path)
				if err := f.Close(); err != nil {
					panic(err)
				}

			case err := <-errs:
				log.Fatalf("failed to generate styleguide: %s", err)

			case <-done:
				if verbose {
					log.Printf("styleguide written to %s\n", outputDir)

					for _, file := range files {
						log.Printf("\t%s\n", file)
					}
				}
				break Generate
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(generateCmd)

	cwd, err := os.Getwd()
	if err != nil {
		log.Fatalf("failed to get working dir: %s", err)
	}

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// generateCmd.PersistentFlags().String("foo", "", "A help for foo")

	generateCmd.Flags().StringVarP(
		&outputType,
		"type", "t",
		string(OutputTypeHtml),
		fmt.Sprintf("The output type (Possible Values: %s)", strings.Join(allOutputTypes, ", ")),
	)

	generateCmd.Flags().StringVarP(&outputDir, "outputDir", "o", cwd, "The output directory")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	generateCmd.Flags().BoolVarP(&verbose, "verbose", "v", false, "Print debug messages")
}
