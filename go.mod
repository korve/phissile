module codeberg.org/korve/phissile

go 1.16

require (
	github.com/iancoleman/strcase v0.1.3 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/rs/xid v1.3.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.0
	github.com/stretchr/testify v1.3.0
	golang.org/x/sys v0.0.0-20191119060738-e882bf8e40c2 // indirect
)
